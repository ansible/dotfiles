---

# https://stackoverflow.com/questions/415403/whats-the-difference-between-bashrc-bash-profile-and-environment
# https://serverfault.com/questions/261802/what-are-the-functional-differences-between-profile-bash-profile-and-bashrc
# https://superuser.com/questions/183870/difference-between-bashrc-and-bash-profile
# https://superuser.com/questions/789448/choosing-between-bashrc-profile-bash-profile-etc

- name: Make sure bash terminal is installed
  ansible.builtin.apt:
    name: bash
    state: present
  become: true

# not always pre-installed on light-weight container images etc.
- name: Make sure bash-completion is installed
  ansible.builtin.apt:
    name: bash-completion
    state: present
  become: true

- name: Install pygmentize (required by "cat" alias)
  ansible.builtin.apt:
    name: python3-pygments
    state: present
  become: true

- name: Create ~/.bashrc and ~/.bash_aliases for {{ ansible_env.USER }}
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    mode: 0644
  # no_log because bash_aliases template looks up passwordstore vars
  no_log: "{% if dotfiles_testing | bool %}false{% else %}true{% endif %}"
  loop:
    - src: bashrc.sh.j2
      dest: "{{ ansible_env.HOME }}/.bashrc"
    - src: bash_aliases.sh.j2
      dest: "{{ ansible_env.HOME }}/.bash_aliases"

- name: Create ~/.bashrc and ~/.bash_aliases for root
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: root
    group: root
    mode: 0644
  become: true
  # no_log because bash_aliases template looks up passwordstore vars
  no_log: "{% if dotfiles_testing | bool %}false{% else %}true{% endif %}"
  loop:
    - src: bashrc.sh.j2
      dest: "/root/.bashrc"
    - src: bash_aliases.sh.j2
      dest: "/root/.bash_aliases"

# additional bash aliases only for workstations
- name: Configure bash_aliases for workstations
  ansible.builtin.lineinfile:
    path: "{{ ansible_env.HOME }}/.bash_aliases"
    insertafter: EOF
    line: "{{ item }}"
    state: present
  when:
    - not (role_constraints | bool)
    # do this for all workstations, whether KVM, LXD or physical
    - group_names is search("workstation")
  loop:
    - "alias ddgyt='BROWSER=mpv ddgr site:youtube.com'"
    # alias for Insect CLI scientific calculator via ssh on luxor
    # https://askubuntu.com/questions/810098/why-doesnt-my-alias-work-over-ssh
    # https://www.man7.org/linux/man-pages/man1/ssh.1.html
    # https://www.gnu.org/software/bash/manual/bash.html#Invoking-Bash
    - "alias insect='ssh luxor -t bash -ci insect'"

# this if-clause just led to "Could not open a connection to your authentication agent.",
# despite ps reporting multiple pre-existing ssh-agent processes. Weird.
# runningagents=$(ps -ef | grep "ssh-agent" | grep -v "grep" | wc -l)
#   if [ $runningagents -eq 0 ]; then
#     eval $(ssh-agent)
#   fi
# I have tried this alternative solution using gpg-agent
# https://blog.lohr.dev/key-management
# It's no good - wants to force us to "protect" our SSH keys with a new password
# to place them in the gpg keyring. Typing another password is pointless.
# - name: Keep ssh-agent alive to load our Git identity ssh keys
#   ansible.builtin.blockinfile:
#     path: "{{ ansible_env.HOME }}/.bashrc"
#     # https://serverfault.com/a/1020834
#     # https://unix.stackexchange.com/questions/73605/how-can-i-silence-ssh-agent
#     # https://superuser.com/questions/737657/silence-ssh-add
#     block: |
#       eval $(ssh-agent) &>/dev/null
#       ssh-add -q {{ ansible_env.HOME }}/.ssh/id_ed25519
#       ssh-add -q {{ ansible_env.HOME }}/.ssh/id_rsa

#     insertafter: "### SSH agent"
#   # I'm primarily trying to make VSCodium's built-in git support to work
#   # with my key identities, so restrict to workstations for now
#   when: group_names is search("workstation")


- name: Customise ~/.profile
  when: not (role_constraints | bool)
  block:

    - name: Overwrite user's ~/.profile
      ansible.builtin.copy:
        src: profile.sh
        dest: "{{ ansible_env.HOME }}/.profile"
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
        mode: 0644

    # note that the TexLive PATH is defined in /etc/profile (in the texlive role)
    - name: "Add CHEPEC's directory of bash scripts to our $PATH"
      ansible.builtin.lineinfile:
        path: "{{ ansible_env.HOME }}/.profile"
        line: "{{ item }}"
        insertafter: EOF
        state: present
      # On luxor and all physical workstations
      when: >
        group_names is search("workstation") or
        inventory_hostname == "luxor"
      loop:
        - 'export PATH="$HOME/chepec/chetex/common/bash:$PATH"'
      become: true
      become_user: '{{ ansible_env.USER }}'
  # END OF BLOCK


# these settings can be applied to any host, I see no reason to discriminate
# export SYSTEMD_LESS="FRXMK" continues to throw that log error, senast x230t#
# please verify correct syntax before re-enabling
# - name: "Create global environment settings"
#   ansible.builtin.lineinfile:
#     path: "{{ item.path }}"
#     line: "{{ item.line }}"
#     insertafter: EOF
#     owner: "{{ item.owner }}"
#     group: "{{ item.group }}"
#     state: present
#   loop:
#     # systemctl: fold long lines in output (systemctl uses less editor)
#     # https://serverfault.com/a/1067315
#     # /etc/bash.bashrc more useful than /etc/profile but still did not work
#     # for "sudo systemctl ..."
#     # https://unix.stackexchange.com/questions/506171/debian-linux-setting-an-environment-variable-for-all-users
#     # https://www.tecmint.com/set-unset-environment-variables-in-linux/
#     # /etc/environment is the only place that took effect for all users, and
#     # also when running "sudo systemctl ..."
#     # NOTE: syslog has been observed to show the message:
#     # "snapd.service: ignoring invalid environment assignment 'export SYSTEMD_LESS=FRXMK': /etc/environment"
#     # might be wise to confirm that this setting actually has the intended effect
#     # of line-breaking systemctl output
#     - path: "/etc/environment"
#       line: 'export SYSTEMD_LESS="FRXMK"'
#       owner: root
#       group: root
#   become: true

# Symlink ~/chepec and friends to {{ bay_root_path }}/taha/chepec and friends
# ~/chepec is the only important one, because other scripts expect to find ~/chepec
# note: the case for symlink_chepec_etal == FALSE is tasked in the unison role
- name: Symlink ~/chepec to {{ bay_root_path }}/taha/chepec
  ansible.builtin.file:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    force: yes # because the src may not exist yet
    state: link
  when:
    - not (role_constraints | bool)
    - >
      group_names is search("workstation") or
      inventory_hostname == "luxor"
    - symlink_chepec_etal | bool
  loop:
    - src: "{{ bay_root_path }}/taha/chepec"
      dest: "{{ ansible_env.HOME }}/chepec"
    - src: "{{ bay_root_path }}/taha/sites"
      dest: "{{ ansible_env.HOME }}/sites"

- name: "Symlink dirs from $HOME to DUNE (only asks2)"
  ansible.builtin.file:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    force: yes # because the src may not exist yet
    state: link
  loop:
    - { src: "/media/DUNE/chepec",   dest: "{{ ansible_env.HOME }}/chepec" }
    - { src: "/media/DUNE/projects", dest: "{{ ansible_env.HOME }}/projects" }
    - { src: "/media/DUNE/sites",    dest: "{{ ansible_env.HOME }}/sites" }
  when: inventory_hostname == "asks2"

- name: Create ~/.bash_functions
  ansible.builtin.template:
    src: bash_functions.sh.j2
    dest: "{{ ansible_env.HOME }}/.bash_functions"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    mode: 0644
  # this matches any group name with the string "workstation" in it
  when: >
    group_names is search("workstation") or
    ansible_play_name is search("zhutop")

- ansible.builtin.import_tasks: bash/nanorc.yml
  when: not (role_constraints | bool)

# fuzzy finder
- ansible.builtin.import_tasks: bash/fzf.yml
  tags: bash-fzf

# Bash functions

## List the latest commit from all git repos

Can we use bash to find all git repos under a particular path? As it turns out, yes.
```
find ~/chepec -iname ".git" -type d
```

Returns the path to all directories that contain a subdirectory `.git` (aka git repositories).
Using that as a starting point, it should be possible to collect information from *all* git repos.

Latest commit in all git repos? Yes, doable:

```
taha@luxor:/media/bay/taha/chepec
$ find chetex/common/R -iname ".git" -type d -exec git -C {} ls -1 \;
```
(note that `-C` must precede `ls`, else you get weird errors).

But for this to be useful, I need each line to also show at least the repo name (perhaps the full path).

This shows the git log and the repo path on the same line, but the price is no more formatting/colors!
```
taha@luxor:/media/bay/taha/chepec
$ find chetex/common/R -iname ".git" -type d  -exec bash -c "git -C {} lf -1 | tr '\n' ' '; echo ' {}' | sed 's+/.git++'" \;
```

The below in combination with a git alias in `~/.gitconfig`: `lf = log --pretty=format:"%ci\\ %C(yellow)%h\\ %Creset%s%Cblue" --decorate --after={2021-07-01}`:

```
$ find /media/bay/taha/chepec/chetex/common/R -iname ".git" -type d  -exec bash -c "git -C {} lf -1 | tr '\n' ' '; echo ' {}' | sed 's+/.git++'" \;
```

I am thinking of one more tweak. Only echo the path if git log actually showed output? 
At first blush, appears hard to achieve given the current structure of the command...

Ok, so could we sort all the output based on the dates? Yes! 
Also, turns out that hiding all lines without git log output is easier than I though -- just pipe to grep, utilising the fact that those lines start with a single space character (and artefact of our `echo " {}"` above):
```
$ find /media/bay/taha/chepec/chetex/common/R -iname ".git" -type d  -exec bash -c "git -C {} lf -1 | tr '\n' ' '; echo ' {}' | sed 's+/.git++'" \; | grep -v "^ " | sort -t- --reverse -k 1.4 -k 2.2 -k 3.2 -k 4.2 -k 5.2 -k 6.2 -
```

Explicit git log command (this removes dependency on `~/.gitconfig`):
```
$ find /media/bay/taha/chepec -iname ".git" -type d  -exec bash -c "git -C {} log --date=format:'%Y-%m-%d %H:%M:%S' --pretty='format:%cd %s' --after={2021-07-01} -1 | tr '\n' ' '; echo ' {}' | sed 's+/.git++'" \; | grep -v "^ " | sort -t- --reverse -k 1.4 -k 2.2 -k 3.2 -k 4.2 -k 5.2 -k 6.2 -
```

I can observe two immediate issues:

+ produces error messages (but moves on) if git repo path contains spaces, 
+ produces error message (but moves on) if git repo contains no commits. 

The latter error is expected. But I'm not sure which part of the command cannot handle paths with spaces?


### Notes

+ https://stackoverflow.com/questions/19345872/how-to-remove-a-newline-from-a-string-in-bash
+ https://stackoverflow.com/questions/5119946/find-exec-with-multiple-commands
+ https://www.codeblocq.com/2016/02/Best-git-log-aliases/
+ https://devhints.io/git-log-format
+ https://www.baeldung.com/linux/find-exec-command
+ https://www.element84.com/blog/using-git-log-to-show-last-months-commits
+ https://unix.stackexchange.com/questions/487823/sort-lines-according-to-date-and-time
+ https://stackoverflow.com/questions/7853332/how-to-change-git-log-date-formats


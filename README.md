# dotfiles

This Ansible role configures the so-called dotfiles (`.bashrc` and friends)
for the current user (and an optional second user account).

## git configuration

Since git version 2.13 (we are on 2.17 now), we can use *conditional `include`*
to configure different identities by folder. This could be useful.

+ https://lazybear.io/posts/git-config-multiple-identities

I already make use of `git-prompt.sh` from `git/contrib`. But there are more
nuggets in git's contrib that could be useful.

+ https://tylercipriani.com/blog/2023/12/31/git-contrib


## bashrc

### Some considerations regarding bash history

I started off with these settings in `~/.bashrc`, settings which
I had used since I moved to Ansible playbook management of my workstation:

```
# don't put duplicate lines or lines starting with space in the history
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# leaving empty means set history to infinite
HISTSIZE=
HISTFILESIZE=
```

This has served my well. There was one thing I thought I might improve,
and that's sharing of history between concurrent terminals on the same
host.

Quite often, I'd move from one open terminal, to another (say when I switched
workspaces in i3), and be annoyed that the long command I just typed in the
terminal on workspace 1 was not visible in the history of the terminal on
workspace 2.

So what I am trying to achieve at the moment, is a shared history
across all terminals. So if I run a command in terminal 1, then go to an already
opened terminal 2, and press arrow Up, the command I just ran in terminal 1
should show up.

**Well, almost**. You have to press `Enter` (or any key really, the point
is you have to refresh the prompt) before the history from the other
terminal(s) becomes available in your current terminal.

This is achievable, but requires redefining `PROMPT_COMMAND`, which means that we are
adding time (albeit slight) to a command that executes after *each and every
terminal command*.

> The amount of processing work between each command is proportional
> to the size of the history file.
https://unix.stackexchange.com/q/1288/411416 (comment by Stéphanie Gourichon)

For reference, my `.bash_history` file on `luxor` currently has just over
16000 lines (with many hundreds of duplicated lines).

~~For now, I'm inclined not to care about duplicates in the history file.~~
~~Perhaps later, I will look into cleaning out duplicate lines using~~
~~`HISTCONTROL=erasedups` or similar.~~
I think a more robust deduplication can be achieved using awk. I have added
the following command to my `totalsync.sh` script (I was trying to also sync
history between workstations, but that is a harder problem):
```
tac $HISTFILE | awk '!entry[$0]++' | tac | sponge $HISTFILE
```

+ https://unix.stackexchange.com/a/179852/411416
+ https://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows
+ https://unix.stackexchange.com/questions/18212/bash-history-ignoredups-and-erasedups-setting-conflict-with-common-history
+ https://unix.stackexchange.com/questions/48713/how-can-i-remove-duplicates-in-my-bash-history-preserving-order
+ https://superuser.com/questions/1096533/how-to-keep-and-aggregate-bash-history-across-multiple-machines
+ https://old.reddit.com/r/awk/comments/bvn4f2/can_awk_process_a_file_backwards
+ https://www.digitalocean.com/community/tutorials/how-to-use-bash-history-commands-and-expansions-on-a-linux-vps

Alright, so I'm going to start by trying a combination
of the SE answers/comments and the DO tutorial, like this:

```
shopt -s histappend
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
```

Explanation of `history` flags:
`-a`: append the new history lines (history lines entered since
      the beginning of the current Bash session) to the history file.
`-c`: Clear the history list. This can be combined with the other
      options to replace the history list completely.
`-r`: Read the current history file and append its contents to the history list.

This works well as a fundament.
This role also configures fuzzy search for history (`fzf`) and as mentioned above
deduplication of the history file happens every time my custom sync script runs.

There are other ways to skin this cat, though.
For example, [sharing history between sessions *without* interleaving](https://unix.stackexchange.com/questions/421868/share-shell-history-across-sessions-without-interleaving-it)
(my approach interleaves).

Note that sharing history between computers is a harder problem.
Unison cannot handle it (I tried) since both files are liable to change *differently*
since last sync. There is [bashhub](https://github.com/nicksherron/bashhub-server),
but it comes with lot of overhead.
Could perhaps symlink the history file into Nextcloud,
or [tweak the `PROMPT_COMMAND` definition](https://superuser.com/a/1099010) (looks
complicated at first blush).


### How to list the top X commands from bash history

Top 100 commands (strips any arguments before counting):

```
history | sed "s/^[0-9 ]*//" | sed "s/ *| */\n/g" | awk '{print $1}' | sort | uniq -c | sort -rn | head -n 100
```

Here's another way, without stripping the arguments before counting:

```
sort .bash_history | uniq -c | sort -n | tail -n 100
```

Note that this latter command will likely not be useful at all
if the history file does not contain duplicates.

Question: why does it contain duplicates, isn't `HISTCONTROL=ignoreboth`
supposed to avoid duplicate lines in history?


## Refs

+ Consider this [bash history "suggest box" program](https://github.com/dvorka/hstr)
+ or this one https://github.com/ddworken/hishtory
+ and [this history "visualiser"](https://github.com/nate-sys/muc)
+ https://tylercipriani.com/blog/2024/02/25/bash-history
+ https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html
+ https://mahmoudashraf.dev/blog/my-terminal-became-more-rusty
+ https://rachelbythebay.com/w/2023/05/05/dot

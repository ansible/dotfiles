# https://github.com/jpmck/PADD/blob/master/padd.sh
GetFTLData() {
  # Get FTL port number
  ftl_port=$(cat /var/run/pihole-FTL.port)

  # Did we find a port for FTL?
  if [[ -n "$ftl_port" ]]; then
    # Open connection to FTL
    exec 3<>"/dev/tcp/localhost/$ftl_port"

    # Test if connection is open
    if { "true" >&3; } 2> /dev/null; then
      # Send command to FTL
      echo -e ">$1" >&3

      # Read input
      read -r -t 1 LINE <&3
      until [ ! $? ] || [[ "$LINE" == *"EOM"* ]]; do
        echo "$LINE" >&1
        read -r -t 1 LINE <&3
      done

      # Close connection
      exec 3>&-
      exec 3<&-
    fi
  # We didn't...?
  else
    echo "0"
  fi
}

GetSummaryInformation() {
  local summary
  local cache_summary
  summary=$(GetFTLData "stats")
  cache_info=$(GetFTLData "cacheinfo")

  clients=$(grep "unique_clients" <<< "${summary}" | grep -Eo "[0-9]+$")

  domains_being_blocked_raw=$(grep "domains_being_blocked" <<< "${summary}" | grep -Eo "[0-9]+$")
  domains_being_blocked=$(printf "%'.f" "${domains_being_blocked_raw}")

  dns_queries_today_raw=$(grep "dns_queries_today" <<< "$summary" | grep -Eo "[0-9]+$")
  dns_queries_today=$(printf "%'.f" "${dns_queries_today_raw}")

  ads_blocked_today_raw=$(grep "ads_blocked_today" <<< "$summary" | grep -Eo "[0-9]+$")
  ads_blocked_today=$(printf "%'.f" "${ads_blocked_today_raw}")

  ads_percentage_today_raw=$(grep "ads_percentage_today" <<< "$summary" | grep -Eo "[0-9.]+$")
  ads_percentage_today=$(printf "%'.1f" "${ads_percentage_today_raw}")

  latest_blocked=$(GetFTLData recentBlocked)
  top_blocked=$(GetFTLData "top-ads (1)" | awk '{print $3}')
  top_domain=$(GetFTLData "top-domains (1)" | awk '{print $3}')

  read -r -a top_client_raw <<< "$(GetFTLData "top-clients (1)")"
  if [[ "${top_client_raw[3]}" ]]; then
    top_client="${top_client_raw[3]}"
  else
    top_client="${top_client_raw[2]}"
  fi

  if [ ${#latest_blocked} -gt 30 ]; then
    latest_blocked=$(echo "$latest_blocked" | cut -c1-27)"..."
  fi

  if [ ${#top_blocked} -gt 30 ]; then
    top_blocked=$(echo "$top_blocked" | cut -c1-27)"..."
  fi
}

PrintPiholeStats() {
  # note: we are making sure each line is 98 chars long 
  # so that background colouring creates a nice rectangle
  echo "Pi-Hole stats ====================================================================================="
  printf " %-10s%-88s\\n" "Blocking:" "${domains_being_blocked} domains"
  printf " %-10s%-7s%-32s%-10s%-39s\\n" "Pi-holed:" "${ads_percentage_today}%" "(${ads_blocked_today} out of ${dns_queries_today} queries)" "Clients:" "${clients}"
  printf " %-10s%-39s%-10s%-39s\\n" "Top Ad:" "${top_blocked}" "Latest:" "${latest_blocked}"
  printf " %-10s%-39s%-10s%-39s\\n" "Top Dmn:" "${top_domain}" "Top Clnt:" "${top_client}"
}

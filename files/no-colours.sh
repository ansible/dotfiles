# Empty versions of all variables in echo-colours.sh

# Reset
Color_Off=''    # Text Reset
# Regular Colors
Black=''        # Black
Red=''          # Red
Green=''        # Green
Yellow=''       # Yellow
Blue=''         # Blue
Purple=''       # Purple
Cyan=''         # Cyan
White=''        # White
# Bold
BBlack=''       # Black
BRed=''         # Red
BGreen=''       # Green
BYellow=''      # Yellow
BBlue=''        # Blue
BPurple=''      # Purple
BCyan=''        # Cyan
BWhite=''       # White
# Underline
UBlack=''       # Black
URed=''         # Red
UGreen=''       # Green
UYellow=''      # Yellow
UBlue=''        # Blue
UPurple=''      # Purple
UCyan=''        # Cyan
UWhite=''       # White
# Background
On_Black=''     # Black
On_Red=''       # Red
On_Green=''     # Green
On_Yellow=''    # Yellow
On_Blue=''      # Blue
On_Purple=''    # Purple
On_Cyan=''      # Cyan
On_White=''     # White
# High Intensity
IBlack=''       # Black
IRed=''         # Red
IGreen=''       # Green
IYellow=''      # Yellow
IBlue=''        # Blue
IPurple=''      # Purple
ICyan=''        # Cyan
IWhite=''       # White
# Bold High Intensity
BIBlack=''      # Black
BIRed=''        # Red
BIGreen=''      # Green
BIYellow=''     # Yellow
BIBlue=''       # Blue
BIPurple=''     # Purple
BICyan=''       # Cyan
BIWhite=''      # White
# High Intensity backgrounds
On_IBlack=''    # Black
On_IRed=''      # Red
On_IGreen=''    # Green
On_IYellow=''   # Yellow
On_IBlue=''     # Blue
On_IPurple=''   # Purple
On_ICyan=''     # Cyan
On_IWhite=''    # White
